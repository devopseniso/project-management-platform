var express =require('express');
const app = express();
const mongoose=require('mongoose');
const config = require('./dbConfig/db.js')
mongoose.Promise=global.Promise;


mongoose.connect(config.DB,{ useNewUrlParser: true },(err) => {
    if (err){
        console.log('Could not connect to db',err);
    }else{
        console.log('Connected to : database');
    }
});


app.get('/',(req,res) => {
    res.send('made changes');
});
app.listen(8080, () =>{
    console.log('Server listening on port 8080');
});
